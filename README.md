# International Patient Summary (IPS) Terminology

This package repackages the IPS terminology from the Implemenation Guide at https://hl7.org/fhir/uv/ips/STU1.1

## Known Issues

- The ConceptMap [http://hl7.org/fhir/uv/ips/ConceptMap/absence-to-snomed-uv-ips](package/ConceptMap-absence-to-snomed-uv-ips.json) references CodeSystems in the `sourceUri` and `targetUri` reference CodeSystems rather than ValueSets - this is an issue in the source material.
- The ConceptMaps [http://hl7.org/fhir/uv/ips/ConceptMap/loinc-pregnancy-status-to-snomed-ct-uv-ips](package/ConceptMap-loinc-pregnancy-status-to-snomed-ct-uv-ips.json) and [http://hl7.org/fhir/uv/ips/ConceptMap/loinc-smoking-status-to-snomed-ct-uv-ips](package/ConceptMap-loinc-smoking-status-to-snomed-ct-uv-ips.json) reference ValueSets defined in other standards. Some resources referenced are included in this package to make them interpretable.
- The CodeSystem for "ATC" (issued by the WHO, canonical http://www.whocc.no/atc) requires a costly license to acquire and can't be redistributed.

